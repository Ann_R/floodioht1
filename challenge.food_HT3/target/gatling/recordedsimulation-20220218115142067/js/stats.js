var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "55",
        "ok": "55",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "174",
        "ok": "174",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "969",
        "ok": "969",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles1": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles2": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "percentiles3": {
        "total": "724",
        "ok": "724",
        "ko": "-"
    },
    "percentiles4": {
        "total": "853",
        "ok": "853",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 54,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.821",
        "ok": "0.821",
        "ko": "-"
    }
},
contents: {
"req_open-home-page-ba7f9": {
        type: "REQUEST",
        name: "Open Home Page",
path: "Open Home Page",
pathFormatted: "req_open-home-page-ba7f9",
stats: {
    "name": "Open Home Page",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "715",
        "ok": "715",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "969",
        "ok": "969",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "777",
        "ok": "777",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "97",
        "ok": "97",
        "ko": "-"
    },
    "percentiles1": {
        "total": "726",
        "ok": "726",
        "ko": "-"
    },
    "percentiles2": {
        "total": "754",
        "ok": "754",
        "ko": "-"
    },
    "percentiles3": {
        "total": "926",
        "ok": "926",
        "ko": "-"
    },
    "percentiles4": {
        "total": "960",
        "ok": "960",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4,
    "percentage": 80
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 20
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_post-click--sta-13145": {
        type: "REQUEST",
        name: "POST Click 'Start' test",
path: "POST Click 'Start' test",
pathFormatted: "req_post-click--sta-13145",
stats: {
    "name": "POST Click 'Start' test",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "179",
        "ok": "179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "percentiles1": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "percentiles3": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "percentiles4": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_get-select-age-0bca1": {
        type: "REQUEST",
        name: "GET Select age",
path: "GET Select age",
pathFormatted: "req_get-select-age-0bca1",
stats: {
    "name": "GET Select age",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles2": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "percentiles3": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "percentiles4": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_post-click--nex-30ecb": {
        type: "REQUEST",
        name: "POST Click 'Next' 3",
path: "POST Click 'Next' 3",
pathFormatted: "req_post-click--nex-30ecb",
stats: {
    "name": "POST Click 'Next' 3",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "174",
        "ok": "174",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "percentiles1": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles2": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "percentiles3": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "percentiles4": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_get-select-larg-e0fb0": {
        type: "REQUEST",
        name: "GET Select Largest Order",
path: "GET Select Largest Order",
pathFormatted: "req_get-select-larg-e0fb0",
stats: {
    "name": "GET Select Largest Order",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "percentiles2": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "percentiles3": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "percentiles4": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_post-click--nex-21ae3": {
        type: "REQUEST",
        name: "POST Click 'Next' 4",
path: "POST Click 'Next' 4",
pathFormatted: "req_post-click--nex-21ae3",
stats: {
    "name": "POST Click 'Next' 4",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "179",
        "ok": "179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles3": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_get-press-next-e63bf": {
        type: "REQUEST",
        name: "GET Press Next",
path: "GET Press Next",
pathFormatted: "req_get-press-next-e63bf",
stats: {
    "name": "GET Press Next",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "179",
        "ok": "179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles2": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles3": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "percentiles4": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_post-click--nex-a8936": {
        type: "REQUEST",
        name: "POST Click 'Next' 5",
path: "POST Click 'Next' 5",
pathFormatted: "req_post-click--nex-a8936",
stats: {
    "name": "POST Click 'Next' 5",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "percentiles1": {
        "total": "179",
        "ok": "179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "percentiles3": {
        "total": "204",
        "ok": "204",
        "ko": "-"
    },
    "percentiles4": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_get-generate-to-87bf8": {
        type: "REQUEST",
        name: "Get Generate Token",
path: "Get Generate Token",
pathFormatted: "req_get-generate-to-87bf8",
stats: {
    "name": "Get Generate Token",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "195",
        "ok": "195",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "182",
        "ok": "182",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "percentiles1": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "182",
        "ok": "182",
        "ko": "-"
    },
    "percentiles3": {
        "total": "192",
        "ok": "192",
        "ko": "-"
    },
    "percentiles4": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_get-enter-token-70b86": {
        type: "REQUEST",
        name: "GET Enter Token",
path: "GET Enter Token",
pathFormatted: "req_get-enter-token-70b86",
stats: {
    "name": "GET Enter Token",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "179",
        "ok": "179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "11",
        "ok": "11",
        "ko": "-"
    },
    "percentiles1": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles2": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "percentiles3": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    },"req_post-click--nex-7596c": {
        type: "REQUEST",
        name: "POST Click 'Next' done",
path: "POST Click 'Next' done",
pathFormatted: "req_post-click--nex-7596c",
stats: {
    "name": "POST Click 'Next' done",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "percentiles1": {
        "total": "180",
        "ok": "180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles3": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "percentiles4": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.075",
        "ok": "0.075",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
