//package challengeFlood

import io.gatling.core.Predef._
import io.gatling.http.Predef._

package object requests {

  val headers_0 = Map(
    "Sec-Fetch-Dest" -> "document",
    "Sec-Fetch-Mode" -> "navigate",
    "Sec-Fetch-Site" -> "none",
    "Sec-Fetch-User" -> "?1",
    "Upgrade-Insecure-Requests" -> "1")

  val headers_1 = Map(
    "Origin" -> "https://challenge.flood.io",
    "Sec-Fetch-Dest" -> "document",
    "Sec-Fetch-Mode" -> "navigate",
    "Sec-Fetch-Site" -> "same-origin",
    "Sec-Fetch-User" -> "?1",
    "Upgrade-Insecure-Requests" -> "1")

  val headers_5 = Map(
    "Accept" -> "*/*",
    "Sec-Fetch-Dest" -> "empty",
    "Sec-Fetch-Mode" -> "cors",
    "Sec-Fetch-Site" -> "same-origin",
    "X-Requested-With" -> "XMLHttpRequest")


  object HomePage {
    val openHomePage = exec(http("Open Home Page")
      .get("/")
      .headers(headers_0)
      .check(status.not(404))
      .check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"").find.saveAs("authenticity_token"))
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id")))
  }

  object TestQuestions {
    val startTest = exec(http("POST Click 'Start' test")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "1")
      .formParam("commit", "Start")
      .check(status.is(302)))

    val selectAge = exec(http("GET Select age")
      .get("/step/2")
      .headers(headers_0)
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
      .check(regex(".*age.*value=\"(.*?)\"").findRandom.saveAs("age")))

    val confirmSelectedAge = exec(http("POST Click 'Next' 3")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "2")
      .formParam("challenger[age]", "${age}")
      .formParam("commit", "Next")
      .check(status.is(302)))

    val selectLargestOrder = exec(http("GET Select Largest Order")
      .get("/step/3")
      .headers(headers_0)
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
      .check(css(".collection_radio_buttons").findAll.transform(list => list.map(_.toInt).max).saveAs("largest_order"))
      .check(regex(".*order_selected.*value=\"(.*?)\"").find.saveAs("order_selected")))

    val confirmLargestOrder = exec(http("POST Click 'Next' 4")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "3")
      .formParam("challenger[largest_order]", "${largest_order}")
      .formParam("challenger[order_selected]", "${order_selected}")
      .formParam("commit", "Next")
      .check(status.is(302)))

    val clickNextButton = exec(http("GET Press Next")
      .get("/step/4")
      .headers(headers_0)
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id")))

    val confirmNextButton = exec(http("POST Click 'Next' 5")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "4")
      .formParam("challenger[order_1]", "1645036237")
      .formParam("challenger[order_2]", "1645036237")
      .formParam("challenger[order_11]", "1645036237")
      .formParam("challenger[order_9]", "1645036237")
      .formParam("challenger[order_9]", "1645036237")
      .formParam("challenger[order_11]", "1645036237")
      .formParam("challenger[order_10]", "1645036237")
      .formParam("challenger[order_10]", "1645036237")
      .formParam("challenger[order_12]", "1645036237")
      .formParam("challenger[order_16]", "1645036237")
      .formParam("commit", "Next")
      .resources(http("Get Generate Token")
        .get("/code")
        .headers(headers_5)
        .check(jsonPath("$.code").find.saveAs("code")))
      .check(status.is(302)))

    val enterToken = exec(http("GET Enter Token")
      .get("/step/5")
      .headers(headers_0)
      .check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
      .check(status.is(200)))

    val confirmEnteredToken = exec(http("POST Click 'Next' done")
      .post("/start")
      .headers(headers_1)
      .formParam("utf8", "✓")
      .formParam("authenticity_token", "${authenticity_token}")
      .formParam("challenger[step_id]", "${step_id}")
      .formParam("challenger[step_number]", "5")
      .formParam("challenger[one_time_token]", "${code}")
      .formParam("commit", "Next")
      .check(status.is(302)))
  }

}
