package challengeFlood

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import requests._

class RecordedSimulation extends Simulation {

  val environment = System.getProperty("apiUrl", "https://challenge.flood.io")
  val th_min = 1
  val th_max = 2
  val test_duration = System.getProperty("duration", "60").toInt
  val test_users = System.getProperty("users", "5").toInt

  val httpProtocol = http
    .baseUrl(environment)
    .inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*css.*""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*"""), WhiteList())
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
    .userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:96.0) Gecko/20100101 Firefox/96.0")
    .disableFollowRedirect

  val scn = scenario("RecordedSimulation")
    .exec(HomePage.openHomePage)
    .pause(th_min, th_max)
    .exec(TestQuestions.startTest)
    .pause(th_min, th_max)
    .exec(TestQuestions.selectAge)
    .pause(th_min, th_max)
    .exec(TestQuestions.confirmSelectedAge)
    .pause(th_min, th_max)
    .exec(TestQuestions.selectLargestOrder)
    .pause(th_min, th_max)
    .exec(TestQuestions.confirmLargestOrder)
    .pause(th_min, th_max)
    .exec(TestQuestions.clickNextButton)
    .pause(th_min, th_max)
    .exec(TestQuestions.confirmNextButton)
    .pause(th_min, th_max)
    .exec(TestQuestions.enterToken)
    .pause(th_min, th_max)
    .exec(TestQuestions.confirmEnteredToken)
    .pause(th_min, th_max)

  setUp(
    scn.inject(
      rampUsers(test_users).during(test_duration))
  ).assertions(
    global.responseTime.max.lt(969),
    global.successfulRequests.percent.gt(95)
  ).protocols(httpProtocol)
}